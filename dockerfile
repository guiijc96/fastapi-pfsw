FROM python:3.9

LABEL MAINTAINER Transpetro - INFRA/ARQIS "infrati.@transpetro.com.br"

EXPOSE 8080 8443

ENV APP_USER="webadmin" \
    APP_USER_HOME="/home/${APP_USER}" \
    APP_PATH=/opt/app/

ENV TZ="America/Sao_Paulo"

USER root

RUN apt update
RUN apt install -y build-essential unixodbc-dev libffi-dev libnsl-dev

COPY . ${APP_PATH}

WORKDIR ${APP_PATH}
RUN python3 -m pip install --upgrade pip
RUN pip install --no-cache-dir --no-warn-script-location -r requirements.txt

CMD ["python3", "main.py"]