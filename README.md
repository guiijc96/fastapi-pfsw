# API Demo 
- Linguagem: Python
- Versão: 3.9

A aplicação consiste em dois recursos de acesso distintos.
Sendo eles uma conexão de ***status com o cluster trino*** definido no arquivo .env referente ao ambiente utilizado e outro com ***arquivo json utilizado como base de dados*** contigo na pasta ***mockin_back***.

Os arquivos nomeados exemplo1 são referentes as operações e controles com base no arquivo de base de dados json.
Os arquivos nomeados exemplo2 são referentes as operações e controles com base na conexão sqlachemy ou sqlalchemy+pandas no cluster trino referente ao ambiente.

### Arquivos .env
Dentro do path ***app/config*** existem 3 arquivos .env que serão usados em distintos caso, de acordo com o local de execução.
- production
- testing
- developer

O arquivo utilizado é automaticamente setado de acordo com a variável de ambiente ***AMBIENT**, que deve conter o nome de um dos arquivos citados acima.

### MVC
Como design pattern, foi escolhido o padrão MVC por se tratar de um padrão de desenvolvimento de APIs já consolidado no mercado e de fácil entendimento. Facilitando a organização do código criado.

Dentro da nossa estrutura, temos as seguintes pastas e suas respectivas funções
- APP : Contém todo o código fonte da aplicação.
    - config : Contém o arquivo de carga das variáveis da aplicação e arquivos .env de ambientes.
    - controllers : Contém os controladores dos recursos que serão disponibilizados pela API.
    - database : Contém os arquivos de conexão com as bases de dados e pasta com as queries que devem ser executadas.
    - models : Contém os arquivos de CRUDs dos recursos, regra de negócio para criação, update, delete e listagem de informações.
    - packages : Arquivos extras para facilitar o uso de alguma feature ou funções genéricas de uso geral.
    - routes : Contém os arquivos de rotas e também a pasta contendo middlewares que serão os intermediários entre cada requisição.
    - schemas : Contém os arquivos de modelo de retorno de cada recurso disponibilizado.

### Docker/Kubernetes
A intessão é sempre utilizar containers para nossas aplicações, devido a isso acompanha arquivo dockerfile para build da imagem na raiz desse projeto.

> A imagem poderá variar de acordo com a necessidade de cada aplicação, este é só um demo.