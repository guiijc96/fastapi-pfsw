# Dependencias basicas
import uvicorn
from fastapi import FastAPI
from fastapi_health import health
from os import getenv
from sys import path
from os.path import dirname, abspath

# adiciona o path base da aplicacao para imports personalizados
path.append(dirname(abspath(__file__)))

# importacoes personalizadas da aplicacao
from app.config.load_config import config
from app.routes.router import Routes
from app.packages.healthcheck import healthy_conditions


app = FastAPI()
app.add_api_route("/health", health([healthy_conditions]))

# adiciono o middleware
# app.add_middleware(MiddlewareAPIM)

# instancio e adiciono as rotas do arquivo de rotas
app.include_router(Routes)

if __name__ == "__main__":
    uvicorn.run(
        config['default'].FLASK_APP
        , host='0.0.0.0'
        , reload=config['default'].RELOAD
        , access_log=config['default'].ACCESS_LOG
        , port=config['default'].APP_PORT
        , debug=config['default'].DEBUG
    )