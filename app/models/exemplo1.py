# Dependencias Basicas
from sys import path
from os.path import dirname, join, abspath
from math import ceil

# Dependencias Essenciais
from datetime import datetime, timedelta
from sqlalchemy import text
import json
import pandas as pd

# path inicial da aplicacao
root_path = join(dirname(abspath(__file__)), '..', '..')
# Dependencias Personalizadas
path.append(root_path)
from app.database.db import engine
from app.packages.cache import Cache
from app.packages.func import default_json

class Produto:
    def __init__(self):
        self.db = None
        self.cached=False
        self.key = 'Produtos'
        self.cache_data = Cache()
        self.file_path = join(root_path, 'mockin_back', 'produtos.json')
        
    def __del__(self):
        if self.db is not None:
            self.db.close()
    
    def get_cache(self, key):
        return self.cache_data._get(key)

    def set_cache(self, key, values):
        valuesset = json.dumps(values,default=default_json)
        self.cache_data._set(key, valuesset)
    
    def valid_cache(self,chave_funcionario):
        data_cache = self.get_cache(self.key.format(chave_funcionario))
        result = json.loads(data_cache) if data_cache is not None else {'timeout_cache': (datetime.now()-timedelta(minutes=15)).strftime('%d/%m/%Y %H:%M:%S')}
        
        if datetime.strptime(result['timeout_cache'], '%d/%m/%Y %H:%M:%S') < datetime.now():
            return False
        return result
    
    def _list(self, codigoProduto=None):
        try:
            produtoList = json.loads(open(self.file_path, 'r', encoding='utf-8').read())
            if codigoProduto is None:
                return {
                        "status": 200
                        , "total_registros": len(produtoList['produtos'])
                        , "items": produtoList['produtos']
                    }
            for produto in produtoList['produtos']:
                if produto['_id'] == codigoProduto:
                    return {
                        "status": 200
                        , "item": produto
                    }
        except Exception as e:
            return {
                "status": 505
                , "message": "Erro interno"
                , "detail_exception": e
            }
    
    def _createItem(self, produtoItem):
        # vcache = False if self.cached else self.valid_cache(self.key)
        # if vcache != False and self.cached == True:
        #     return vcache
        try:
            produtoList = json.loads(open(self.file_path, 'r', encoding='utf-8').read())
            produtoItem = vars(produtoItem)
            if produtoItem in produtoList['produtos']:
                return {
                    "status": 422
                    , "message": "Produto Já Cadastrado"
                    , "item": produtoItem
                }
                
            _ids = [produto['_id'] for produto in produtoList['produtos']]
            produtoItem['_id'] = max(_ids)+1
            produtoList['produtos'].append(produtoItem)
            
            with open(self.file_path,'w') as file:
                file.write(json.dumps(produtoList,indent=4))
            
            return {
                "status": 200
                , "message": "Produto Cadastrado com Sucesso"
                , "item": produtoItem
            }
        except Exception as e:
            return {
                "status": 505
                , "message": "Erro interno"
                , "detail_exception": e
            }
        
    def _updateParcialProduto(self, codigoProduto, partialProduto):
        try:
            produtoList = json.loads(open(self.file_path, 'r', encoding='utf-8').read())
            for produto in produtoList['produtos']:
                if produto['_id'] == codigoProduto:
                    produto.update([(key, partialProduto[key]) for key in partialProduto.keys() if key != '_id' and partialProduto[key] is not None])
                    break
            with open(self.file_path,'w') as file:
                file.write(json.dumps(produtoList,indent=4))
            return {
                "status": 200
                , "message": 'Produto Atualizado'
                , "produto_atualizado": produto
            }
        except Exception as e:
            return {
                "status": 505
                , "message": "Erro interno"
                , "detail_exception": e
            }
        return True
    
    def _updateTotalProduto(self, codigoProduto, produtoItem):
        try:
            produtoList = json.loads(open(self.file_path, 'r', encoding='utf-8').read())
            for idx, produto in enumerate(produtoList['produtos']):
                if produto['_id'] == codigoProduto:
                    ant = produto
                    produto = produtoItem
                    produtoList['produtos'][idx] = produto
                    produtoList['produtos'][idx]['_id'] = ant['_id']
                    break
            with open(self.file_path,'w') as file:
                file.write(json.dumps(produtoList,indent=4))        
            return {
                "status": 200
                , "message": 'Produto Atualizado'
                , "produto_antigo": ant
                , "produto_atualizado": produto
            }
        except Exception as e:
            return {
                "status": 505
                , "message": "Erro interno"
                , "detail_exception": e
            }
    
    def _deleteProduto(self, codigoProduto):
        try:
            produtoList = json.loads(open(self.file_path, 'r', encoding='utf-8').read())
            for idx,produto in enumerate(produtoList['produtos']):
                if produto['_id'] == codigoProduto:
                    produtoList['produtos'].pop(idx)
                    
            with open(self.file_path,'w') as file:
                file.write(json.dumps(produtoList,indent=4))
            return {
                "status": 200
                , "message": "Produto Removido"
            }
        except Exception as e:
            return {
                "status": 505
                , "message": "Erro interno"
                , "detail_exception": e
            }