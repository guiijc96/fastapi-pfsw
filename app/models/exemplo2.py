# Dependencias Basicas
from sys import path
from os.path import dirname, join, abspath
from math import ceil

# Dependencias Essenciais
from datetime import datetime, timedelta
from sqlalchemy import text
import json
import pandas as pd

# path inicial da aplicacao
root_path = join(dirname(abspath(__file__)), '..', '..')
path.append(root_path)

# Dependencias Personalizadas
from app.database.db import engine
from app.packages.cache import Cache
from app.packages.func import default_json

class TrinoCluster:
    def __init__(self):
        self.db = None
        self.cached=False
        self.key = 'TrinoCluster'
        self.cache_data = Cache()
        self.query_path = join(root_path, 'app', 'database', 'queries', 'trinocluster','status.sql')
        
    def __del__(self):
        if self.db is not None:
            self.db.close()
    
    def get_cache(self, key):
        return self.cache_data._get(key)

    def set_cache(self, key, values):
        valuesset = json.dumps(values,default=default_json)
        self.cache_data._set(key, valuesset)
    
    def valid_cache(self,chave_funcionario):
        data_cache = self.get_cache(self.key.format(chave_funcionario))
        result = json.loads(data_cache) if data_cache is not None else {'timeout_cache': (datetime.now()-timedelta(minutes=15)).strftime('%d/%m/%Y %H:%M:%S')}
        
        if datetime.strptime(result['timeout_cache'], '%d/%m/%Y %H:%M:%S') < datetime.now():
            return False
        return result
    
    def _status_cluster(self):
        try:
            db_trino = engine().connect()
            
            df_trino = pd.read_sql(
                sql=open(join(self.query_path), 'r', encoding='utf-8' ).read()
                , con=db_trino
            )
            
            return {
                "status": 200
                , 'total_items': len(df_trino.index)
                , 'items': json.loads(df_trino.to_json(force_ascii=False,orient='records',date_format='iso'))
            }
        except Exception as e:
            return {
                "status": 505
                , "message": "Erro interno"
                , "detail_exception": e
            }