# Dependencias Basicas
from sys import path
from os.path import dirname, join, abspath
from math import ceil

# path inicial da aplicacao
root_path = join(dirname(abspath(__file__)), '..', '..')
# Dependencias Personalizadas
path.append(root_path)
from app.models.exemplo2 import TrinoCluster

class TrinoClusterController:
    def get_status(self):
        trino_cluster = TrinoCluster()
        result = trino_cluster._status_cluster()
        return result