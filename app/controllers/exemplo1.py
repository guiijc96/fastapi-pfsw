# Dependencias Basicas
from sys import path
from os.path import dirname, join, abspath
from math import ceil

# path inicial da aplicacao
root_path = join(dirname(abspath(__file__)), '..', '..')
# Dependencias Personalizadas
path.append(root_path)
from app.models.exemplo1 import Produto

class ProdutoController:
    def get(self, codigo_produto=None):
        print(f'codigo: {codigo_produto}')
        produto = Produto()
        result = produto._list() if codigo_produto is None else produto._list(codigo_produto)
        return result
    
    def create(self, payload_produto):
        produto = Produto()
        result = produto._createItem(payload_produto)
        return result
    
    def put(self,codigo_produto, payload_produto):
        produto = Produto()
        result = produto._updateTotalProduto(codigo_produto,payload_produto)
        return result
    
    def patch(self,codigo_produto, payload_produto):
        produto = Produto()
        result = produto._updateParcialProduto(codigo_produto,payload_produto)
        return result
    
    def delete(self,codigo_produto):
        produto = Produto()
        result = produto._deleteProduto(codigo_produto)
        return result