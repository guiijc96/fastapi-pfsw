# Dependencias Basicas
from os.path import dirname, abspath, join
from sys import path

# Dependencias Essenciais
from fastapi import APIRouter, Request, Path, Depends, Query, status
from fastapi.responses import JSONResponse
from typing import Optional

# Dependias Personalizadas
path.append(join(dirname(abspath(__file__)), '..', '..'))

# Controllers, Models e Schemas Produtos
from app.controllers.exemplo1 import ProdutoController
from app.schemas.exemplo1 import CreateProduto, UpdateProduto, PartialUpdateProduto, DeleteProduto, ListProduto, ProdutoItem, ProdutoException

from app.controllers.exemplo2 import TrinoClusterController
from app.schemas.exemplo2 import StatusTrino, errorTrino

Routes = APIRouter()

@Routes.get('/index')
def index(request: Request):
    """
    Captura o ip da máquina que realizou a solicitação
    """
    client_host = request.client.host
    return f'{client_host}'

@Routes.get(
    '/status_virtualization'
    , response_model = StatusTrino
    , responses={505:{'model':errorTrino}}
)
def status_trino():
    """
    Retorna o status do cluster de virtualização Trino
    """
    response = TrinoClusterController().get_status()
    return JSONResponse(status_code=response['status'], content=response)

@Routes.get(
    '/produtos'
    , response_model=ListProduto
    , responses={505: {'model': ProdutoException}, 422: {'model': ListProduto}}
)
async def list_all_produtos():
    """
    Retorna a lista de todos os produtos contidos no banco de dados.
    obs: banco de dados em arquivo, usado somente para testes do fastapi e realização de template/prototipo
    """
    response = ProdutoController().get()
    return JSONResponse(status_code=response['status'], content=response)

@Routes.get(
    '/produtos/{codigo_produto}'
    , response_model=ListProduto
    , responses={505: {'model': ProdutoException}, 422: {'model': ListProduto}}
)
def list_produto(codigo_produto: int = Path(ge=1, description='Código Indentificador do Produto')):
    """
    Retorna o produto especificado na chamada.
    obs: banco de dados em arquivo, usado somente para testes do fastapi e realização de template/prototipo
    """
    response = ProdutoController().get(codigo_produto)
    return JSONResponse(status_code=response['status'], content=response)

@Routes.post(
    '/produtos'
    , response_model=CreateProduto
    , responses={505: {'model': ProdutoException}, 422: {'model': CreateProduto}}
)
def create_produto(produto: ProdutoItem):
    """
    Cria um novo produto na base de dados.
    obs: banco de dados em arquivo, usado somente para testes do fastapi e realização de template/prototipo
    """
    response = ProdutoController().create(produto)
    return JSONResponse(status_code=response['status'], content=response)

@Routes.put(
    '/produtos/{codigo_produto}'
    , response_model=UpdateProduto
    , responses={505: {'model': ProdutoException}, 422: {'model': UpdateProduto}}
)
def update_produto(codigo_produto: int, produto: ProdutoItem):
    """
    Realiza um update completo no produto.
    obs: banco de dados em arquivo, usado somente para testes do fastapi e realização de template/prototipo
    """
    response = ProdutoController().put(codigo_produto, vars(produto))
    return JSONResponse(status_code=response['status'], content=response)

@Routes.patch(
    '/produtos'
    , response_model=PartialUpdateProduto
    , responses={505: {'model': ProdutoException}, 422: {'model': PartialUpdateProduto}}
)
def partial_update_produto(
    codigo_produto: int
    , produto: ProdutoItem
):
    """
    Realiza um update parcial no produto.
    obs: banco de dados em arquivo, usado somente para testes do fastapi e realização de template/prototipo
    """
    response = ProdutoController().patch(codigo_produto, vars(produto))
    return JSONResponse(status_code=response['status'], content=response)

@Routes.delete(
    '/produtos/{codigo_produto}'
    , response_model=DeleteProduto
    , responses={505: {'model': ProdutoException}, 422: {'model': DeleteProduto}}
)
def delete_produto(codigo_produto: int = Path(ge=1, description='Código Indentificador do Produto')):
    """
    Remove fisicamente o produto.
    obs: banco de dados em arquivo, usado somente para testes do fastapi e realização de template/prototipo.
        -futuramente irei testar com softdelete ou realizar a transferencia para uma base separada.
    """
    response = ProdutoController().delete(codigo_produto)
    return JSONResponse(status_code=response['status'], content=response)


