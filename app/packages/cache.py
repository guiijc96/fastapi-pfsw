# Dependencias Basicas
from os.path import dirname, abspath, isfile, join
from os import getenv
from dotenv import load_dotenv

# Dependencias Essenciais
from redis import Redis

_ENV_FILE = join(dirname(abspath(__file__)), '..', '..', 'app', 'config', f'{getenv("AMBIENT")}.env')
if isfile(_ENV_FILE):
    load_dotenv(dotenv_path=_ENV_FILE)
else:
    print('[warning]: Arquivo de variaiveis de ambiente nao existe')
    
class Cache:
    def __init__(self):
        self.redis_instance = Redis(
            host=getenv(getenv('HOST_REDIS').replace('$','')),
            port=getenv(getenv('PORT_REDIS').replace('$',''))
        )
        
    def _set(self, key, value):
        return self.redis_instance.set(key, value)
    
    def _get(self, key):
        return self.redis_instance.get(key)