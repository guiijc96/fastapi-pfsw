from datetime import datetime, date

def default_json(o):
    if isinstance(o, (date, datetime)):
        return o.isoformat()