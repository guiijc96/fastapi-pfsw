# Dependencias basicas
from sys import path
from os.path import join, dirname, abspath, isfile
from dotenv import load_dotenv
from os import getenv
from datetime import datetime

# adiciona o root path
path.append(join(dirname(abspath(__file__)), '..', '..'))
# instancio as dependencias personalizadas
from app.database.db import engine
from app.packages.cache import Cache
## Conexoes de banco e cache

def db_memcache():
    try:
        redis_instance = Cache().redis_instance
        ping = redis_instance.ping()
        return ping
    except:
        return False
    
def db_virtualization():
    try:
        db = engine().connect()
        db.execute('select current_date')
        db.close()
        return True
    except:
        return False
    
def healthy_conditions():
    response = {
        "dependencies": {
            "memcache_redis": db_memcache()
            , "virtualization": db_virtualization()
        }
    }
    return response