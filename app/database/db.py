# Dependencias Basicas
from dotenv import load_dotenv
from os.path import dirname, abspath, isfile, join
from os import getenv
from sys import path

# Dependencias Essenciais
from sqlalchemy import create_engine

root_path = join(dirname(abspath(__file__)), '..', '..')
# Dependencias Personalizadas
# path.append(root_path)
# from app.packages.kv_transpetro import kv_transp

_ENV_FILE = join(root_path, 'app', 'config', f'{getenv("AMBIENT")}.env')
if isfile(_ENV_FILE):
    load_dotenv(dotenv_path=_ENV_FILE)
else:
    print('[warning]: Arquivo de variaiveis de ambiente nao existe')
    
def engine():
    if getenv("PASSWORD_TRINO") is not None:
        return create_engine(f'trino://{getenv("USER_TRINO")}:{getenv("PASSWORD_TRINO")}@{getenv("HOST_TRINO")}:{getenv("PORT_TRINO")}')
    return create_engine(f'trino://{getenv("USER_TRINO")}@{getenv("HOST_TRINO")}:{getenv("PORT_TRINO")}')