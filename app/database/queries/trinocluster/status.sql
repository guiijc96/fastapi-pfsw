SELECT 
    node_id
    , http_uri
    , node_version
    , coordinator
    , state
 FROM system.runtime.nodes