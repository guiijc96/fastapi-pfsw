# Dependencias basicas
from os import getenv
from dotenv import load_dotenv
from os.path import dirname, abspath, isfile, join

_ENV_FILE = join(dirname(abspath(__file__)), '..', '..', 'app', 'config', f'{getenv("AMBIENT")}.env')
if isfile(_ENV_FILE):
    load_dotenv(dotenv_path=_ENV_FILE)
else:
    print('[warning]: Arquivo de variaiveis de ambiente nao existe')
    
class Config:
    APP_PORT = int(getenv('APP_PORT'))
    FLASK_ENV=getenv('FLASK_ENV')
    FLASK_APP=getenv('FLASK_APP')
    DEBUG=eval(getenv('DEBUG').title())
    TESTING=eval(getenv('TESTING').title())
    RELOAD=eval(getenv('RELOAD').title())
    ACCESS_LOG=eval(getenv('ACCESS_LOG').title())
    
config = {
    'default': Config
}