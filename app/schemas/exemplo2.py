from typing import List, Union
from fastapi import FastAPI
from pydantic import BaseModel

class StatusNodeTrino(BaseModel):
    node_id: str
    http_uri: str
    node_version: str
    coordinator: bool
    state: str
    
class StatusTrino(BaseModel):
    status: int
    total_items: int
    items: List[StatusNodeTrino]
    
class errorTrino(BaseModel):
    status: int
    message: str
    detail_exception: str