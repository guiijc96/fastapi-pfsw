from typing import List, Union
from fastapi import FastAPI
from pydantic import BaseModel

class ProdutoItem(BaseModel):
    _id: int
    nome: Union[str,None]
    descricao: Union[str,None]
    preco: Union[float,None]
    categoria_id: Union[int,None]

class CreateProduto(BaseModel):
    status: int
    item: ProdutoItem

class PartialUpdateProduto(BaseModel):
    status: int
    message: str
    produto_atualizado: ProdutoItem
    
class UpdateProduto(BaseModel):
    status: int
    message: str
    produto_antigo: ProdutoItem
    produto_atualizado: ProdutoItem
    
class ListProduto(BaseModel):
    status: int
    message: str
    total_registros: int
    items: List[ProdutoItem]
    
class DeleteProduto(BaseModel):
    status: int
    message: str
    
class ProdutoException(BaseModel):
    status: int
    message: str
    detail_exception: str